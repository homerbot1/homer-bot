# Homer MergeRequest Acceptance process

The Acceptance process slightly differs whether you are a project's *developer* (or higher) or not.

## Voting system

- The **Score** is computed by comparing the number of :thumbsup: and :thumbsdown: given to the MergeRequest
- Only project *developers* (or higher) are taken into account
- No vote (for a Developer MR) means Score = 0

## If the MergeRequest author has Developer role (or higher)

```mermaid
stateDiagram
    Submitted: CR Submitted
    InReview: In Review
    [*] --> Submitted: A VLC developer submitted
    Submitted --> Rejected: CI NOK\nor\nnot mergeable
    Submitted --> Reviewable: CI OK\nand\nmergeable
    Reviewable --> InReview: Thread opened\nor\nAnother Developer voted
    Reviewable --> Accepted: No vote\nNo thread opened\nduring 72h
    InReview --> Acceptable: All threads resolved\nand\nScore >= 0
    Acceptable --> Accepted: Wait for 24h
    Acceptable --> InReview: A thread has been (re)opened\nor\nScore < 0
    Accepted --> Merged: A Developer launches rebase and merge when pipeline succeeds
    Accepted --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore < 0
    Merged --> [*]
```

## If the MergeRequest author is **NOT** a project's developer

```mermaid
stateDiagram
    Submitted: CR Submitted
    InReview: In Review
    [*] --> Submitted: A VLC developer submitted
    Submitted --> Rejected: CI NOK\nor\nnot mergeable
    Submitted --> Reviewable: CI OK\nand\nmergeable
    Reviewable --> InReview: Thread opened\nor\nAnother Developer voted
    InReview --> Acceptable: All threads resolved\nand\nlen(votes) > 0\nand\n Score > 0
    Acceptable --> Accepted: Wait for 72h
    Acceptable --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore <= 0
    Accepted --> Merged: A Developer launches rebase and merge when pipeline succeeds
    Accepted --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore <= 0
    Merged --> [*]
```

