package main

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	tokenEnvVar          = "GITLAB_PRIVATE_TOKEN"
	rootURLEnvVar        = "GITLAB_API_ROOT_URL"
	defaultAPIRootURL    = "https://code.videolan.org/api/v4"
	acceptedLabel        = "Accepted"
	rejectedLabel        = "Rejected"
	metadataPrefixBase64 = "homerbotmeta-base64:"
	messageTypeReport    = "report"
	messageTypeWelcome   = "welcome"
)

// Metadata represents the json metadata injected in homer-bot messages.
type Metadata struct {
	MessageType  string `json:"message_type"`
	ReportResult string `json:"report_result,omitempty"`
}

var (
	verbose                = false
	debug                  = false
	chatty                 = true
	mrInactiveHours        int64
	mrDevScoreHours        int64
	mrOtherScoreHours      int64
	mrDevDiscussionHours   int64
	mrOtherDiscussionHours int64
)

func logMR(mrID int64, v ...interface{}) {
	log.Println(fmt.Sprintf("[MR %d]", mrID), fmt.Sprint(v...))
}
func logMRVerbose(mrID int64, v ...interface{}) {
	if verbose || debug {
		logMR(mrID, v...)
	}
}

func generateMetadataString(meta Metadata) (string, error) {
	bytes, err := json.Marshal(meta)
	if err != nil {
		return "", err
	}
	return metadataPrefixBase64 + base64.StdEncoding.EncodeToString(bytes), nil
}

func retrieveMetadataFromString(message string, metadata *Metadata) (bool, error) {
	for _, line := range strings.Split(message, "\n") {
		if strings.HasPrefix(line, metadataPrefixBase64) {
			base64Message := strings.TrimPrefix(line, metadataPrefixBase64)
			bytes, err := base64.StdEncoding.DecodeString(base64Message)
			if err != nil {
				return true, err
			}
			return true, json.Unmarshal(bytes, metadata)
		}
	}
	return false, nil
}

// Markdown report
func generateReport(mr MergeRequest, checks []Check) string {
	fullSuccess := true
	details := fmt.Sprintln("### MR acceptance checks details")
	for _, check := range checks {
		details += fmt.Sprintln("* ", check.Markdown())
		if !check.Success {
			fullSuccess = false
		}
	}
	report := fmt.Sprintln("### MR Acceptance result")
	if fullSuccess {
		report += fmt.Sprintln(":tada: This MergeRequest has been **Accepted**! Congratulations.")
	} else {
		report += fmt.Sprintln(":no_entry_sign: This MergeRequest has not been accepted (yet?).")
	}
	// write details, but make them collapsible
	report += fmt.Sprintln("<details><summary>Click here for more details</summary><p>")
	report += fmt.Sprintln("")
	report += fmt.Sprintln(details)
	report += fmt.Sprintln("</p></details>")
	// Add a message identifier (maybe for future use)
	metadata := Metadata{MessageType: messageTypeReport, ReportResult: rejectedLabel}
	if fullSuccess {
		metadata.ReportResult = acceptedLabel
	}
	if meta, err := generateMetadataString(metadata); err == nil {
		report += fmt.Sprintln("<!--")
		report += fmt.Sprintln(meta)
		report += fmt.Sprintln("-->")
	}
	return report
}

func sendWelcomeMessage(mr *MergeRequest, dryrun bool) {
	// Check if we have to add a welcome message
	welcome, err := mr.IsWelcomeMessagePresent()
	if err != nil {
		logMR(mr.IID, "ERROR! Something gone wrong when trying to check the welcome message: ", err.Error())
	} else if !welcome {
		logMR(mr.IID, "Cannot find a welcome message in the MR discussions, should add it!")
		if dryrun {
			logMR(mr.IID, "Dryrun enabled, will not post a welcome message")
		} else {
			welcomeMessage, err := mr.GenerateWelcomeMessage()
			if err != nil {
				logMR(mr.IID, "ERROR while trying to generate a Welcome message", err)
			} else {
				if err := mr.CreateNote(welcomeMessage); err != nil {
					logMR(mr.IID, "ERROR while trying to add a Welcome Message in MR:", err)
				} else {
					logMR(mr.IID, "Welcome message successfully added")
				}
			}
		}
	} else {
		logMR(mr.IID, "Already found a Welcome message in MR, not adding a new one")
	}
}

func main() {
	var projectID int64
	var dryrun bool
	flag.Int64Var(&projectID, "project", 0, "The Gitlab Project ID. Look at the Project Home Page")
	flag.BoolVar(&verbose, "verbose", false, "Verbose mode.")
	flag.BoolVar(&debug, "debug", false, "Debug mode. Shows HTTP requests")
	flag.BoolVar(&dryrun, "dryrun", false, "Do not touch anything - only analyze merge requests.")
	flag.BoolVar(&chatty, "chatty", true, "Homer will send welcome messages to MergeRequests.")
	// Timeout/duration parameters for checks
	flag.Int64Var(&mrInactiveHours, "mr-inactive-hours", 72, "hours before accepting an inactive developer MR")
	flag.Int64Var(&mrDevScoreHours, "mr-devscore-hours", 24, "hours before considering a >=0 vote score for a developer MR is OK")
	flag.Int64Var(&mrOtherScoreHours, "mr-otherscore-hours", 72, "hours before considering a >0 vote score for a external participant MR is OK")
	flag.Int64Var(&mrDevDiscussionHours, "mr-devdiscussion-hours", 24, "hours before considering resolved threads for a developer MR are OK")
	flag.Int64Var(&mrOtherDiscussionHours, "mr-otherdiscussion-hours", 72, "hours before considering resolved threads for an external participant MR are OK")
	flag.Parse()
	if projectID == 0 {
		log.Println("Please provide a Project ID.")
		os.Exit(-1)
	}
	token := os.Getenv(tokenEnvVar)
	if token == "" {
		log.Println(tokenEnvVar, "environment variable not set! Aborting...")
		os.Exit(-1)
	}
	rootURL := os.Getenv(rootURLEnvVar)
	if rootURL == "" {
		rootURL = defaultAPIRootURL
	}
	// Get connected user
	user, err := GetConnectedUser(rootURL, token)
	if err != nil {
		log.Println("Error while retrieving Connected user information: ", err.Error())
		os.Exit(-1)
	}
	log.Println("Connected as", user.Username, "(", user.Name, ")")
	project := Project{ID: projectID, APIRootURL: rootURL, AuthToken: token}
	mrs, err := project.GetMRs(map[string]string{"state": "opened", "wip": "no"})
	if err != nil {
		log.Println("Error while retrieving Project's MRs: ", err.Error())
		os.Exit(-1)
	}
	log.Println("In Project Number ", project.ID)
	numberOfMRs := len(mrs)
	errors := make(chan error, numberOfMRs)
	for _, mr := range mrs {
		go func(mr MergeRequest) {
			if mr.WorkInProgress {
				logMR(mr.IID, "MR ", mr.IID, " is flagged as Work in Progress, ignoring it")
				errors <- nil
				return
			}
			logMR(mr.IID, "Active MR:", mr.IID)
			if chatty {
				sendWelcomeMessage(&mr, dryrun)
				// Replay to direct request
				userNotes, err := mr.GetAllNotesReferingUser(user)
				if err != nil {
					logMR(mr.IID, "ERROR while trying to get all notes refering connection user:", err)
				} else {
					for _, note := range *userNotes {
						logMR(mr.IID, "REQUEST: found a note requesting something: ", note.ID)
					}
				}
			}
			// Analyze MR
			checks := mr.Analyze()
			fullSuccess := true
			var failedCheck Check
			var checkError error = nil
			for _, check := range checks {
				if !check.Success {
					fullSuccess = false
					failedCheck = check
				}
				if check.Error != nil {
					checkError = check.Error
				}
				logMRVerbose(mr.IID, check.String())
			}
			if checkError != nil {
				logMR(mr.IID, "ERROR! Something gone wrong during the MR analyze: ", checkError.Error())
				// Do not perform any operation if an error occured
				errors <- checkError
				return
			} else if fullSuccess {
				logMR(mr.IID, "SUCCESS! MergeRequest is fully compliant!")
			} else {
				logMR(mr.IID, "FAIL! MergeRequest is not fully compliant: ", failedCheck.String())
			}
			if fullSuccess && !mr.HasLabel(acceptedLabel) {
				logMR(mr.IID, "MergeRequest is fully compliant and has no Accept Label! We should do this!")
				if dryrun {
					logMRVerbose(mr.IID, "Bypassing Label addition (dryrun)")
				} else {
					if err := mr.AddLabel(acceptedLabel); err != nil {
						logMR(mr.IID, "ERROR while trying to add a label in MR:", err)
					} else {
						logMR(mr.IID, "Label successfully added")
					}
					if err := mr.CreateNote(generateReport(mr, checks)); err != nil {
						logMR(mr.IID, "ERROR while trying to add a comment in MR:", err)
					} else {
						logMR(mr.IID, "Comment successfully added")
					}

				}
			} else if !fullSuccess && mr.HasLabel(acceptedLabel) {
				logMR(mr.IID, "MergeRequest does not meet the requirements, and has the Accept Label! We should remove it.")
				if dryrun {
					logMRVerbose(mr.IID, "Bypassing Label deletion (dryrun)")
				} else {
					if err := mr.RemoveLabel(acceptedLabel); err != nil {
						logMR(mr.IID, "ERROR while trying to remove a label in MR:", err)
					} else {
						logMR(mr.IID, "Label successfully removed")
					}
				}
			}
			errors <- nil
		}(mr)
	}
	var finalError error = nil
	for i := 0; i < numberOfMRs; i++ {
		if err := <-errors; err != nil {
			finalError = err
		}
	}
	if finalError != nil {
		log.Println("An error has occured during the process!")
		os.Exit(-2)
	} else {
		log.Println("All MRs have been processed.")
		os.Exit(0)
	}
}
