package main

import (
	"fmt"
	"regexp"
	"time"
)

// Check is an internal representation of a check
type Check struct {
	Description string
	Success     bool
	Reason      string
	Error       error
}

func (check *Check) baseString() string {
	s := check.Description
	if check.Reason != "" {
		s += " - Reason: " + check.Reason
	}
	if check.Error != nil {
		s += " - ERROR: " + check.Error.Error()
	}
	return s
}

// String provides a string representation of the Check. Used in logs
func (check *Check) String() string {
	if check.Success {
		return "OK  - " + check.baseString()
	}
	return "NOK - " + check.baseString()
}

// Markdown provides a markdown representation of the Check. Used in gitlab comment
func (check *Check) Markdown() string {
	if check.Success {
		return ":white_check_mark: " + check.baseString()
	}
	return ":x: " + check.baseString()
}

// CheckStatus checks the MR status
func (mr *MergeRequest) CheckStatus() Check {
	return Check{
		"MR should be considered mergeable by Gitlab",
		mr.MergeStatus == MergeStatusCanBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckInactivity checks MR "full" inactivity: no vote, no discussion
func (mr *MergeRequest) CheckInactivity(threshold time.Duration) Check {
	description := fmt.Sprintf("No activity on MR (no review, no vote) and last update is longer than %s", threshold)
	lastUpdate, err := mr.GetLatestVersionTime()
	if err != nil {
		return Check{description, false, "", err}
	}
	logMRVerbose(mr.IID, "Latest MR Version Time:", lastUpdate)
	timeSinceLastUpdate := time.Since(lastUpdate)
	if timeSinceLastUpdate < threshold {
		return Check{description, false, fmt.Sprintf("Last update is %s", timeSinceLastUpdate.Round(time.Minute)), nil}
	}
	_, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes > 0 {
		return Check{description, false, fmt.Sprintf("Dev votes have been found (%d)", votes), nil}
	}
	notes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	if len(*notes) > 0 {
		return Check{description, false, fmt.Sprintf("Discussion threads have been found (%d)", len(*notes)), nil}
	}
	return Check{description, true, fmt.Sprintf("No dev vote or thread has been found, and time since last update is %s", timeSinceLastUpdate.Round(time.Minute)), nil}
}

// CheckDevScore checks the Developer Votes score.
// threshold is the minimal time before validation
// strict means the score should be strictly positive to be validated
func (mr *MergeRequest) CheckDevScore(threshold time.Duration, strict bool) Check {
	description := fmt.Sprintf("Developers Vote Score is high enough for at least %s", threshold)
	if strict {
		description += ", with a minimum of one vote"
	}
	score, votes, lastSignChange, err := mr.DevScore(strict)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes == 0 {
		if strict {
			return Check{description, false, "No Developer vote has been found", nil}
		}
		// if not strict and there is no vote (developer case), consider the check to be ok.
		return Check{description, true, "No Developer vote has been found, score is considered zero", nil}
	}
	var success bool
	var reason string
	lastSignChangeStr := lastSignChange.String()
	if lastSignChange.IsZero() {
		lastSignChangeStr = "never"
	}
	if strict {
		if score <= 0 {
			success = false
			reason = fmt.Sprintf("Score (%d) is not strictly positive", score)
		} else if time.Since(lastSignChange) < threshold {
			success = false
			reason = fmt.Sprintf("Score (%d) has not been strictly positive long enough (last sign change: %s)", score, lastSignChangeStr)
		} else {
			success = true
			reason = fmt.Sprintf("Score (%d) has been strictly positive long enough (last sign change: %s)", score, lastSignChangeStr)
		}
	} else {
		if score < 0 {
			success = false
			reason = fmt.Sprintf("Score (%d) is strictly negative", score)
		} else if time.Since(lastSignChange) < threshold {
			success = false
			reason = fmt.Sprintf("Score (%d) has not been positive long enough (last sign change: %s)", score, lastSignChangeStr)
		} else {
			success = true
			reason = fmt.Sprintf("Score (%d) has been positive long enough (last sign change: %s)", score, lastSignChangeStr)
		}
	}
	return Check{description, success, reason, nil}
}

// CheckDiscussions checks all threads in MR have been resolved
// threshold is the minimal time before validation
func (mr *MergeRequest) CheckDiscussions(threshold time.Duration) Check {
	description := fmt.Sprintf("All Threads should be resolved for at least %s", threshold)
	notes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	lastResolved := time.Time{}
	for _, note := range *notes {
		if !note.Resolved {
			return Check{description, false, fmt.Sprintf("A thread is still [unresolved](#note_%d)", note.ID), nil}
		}
		if note.Author.ID != mr.Author.ID && note.ResolvedBy != nil && note.ResolvedBy.ID == mr.Author.ID {
			// NOTE: should we do something else ? A "Invalid" label on the MR ?
			return Check{description, false, fmt.Sprintf("A thread not started by the MR Author has been closed by the MR Author: [thread](#note_%d)", note.ID), nil}
		}
		// resolved + legit => take updatetime into account
		if note.UpdatedAt.After(lastResolved) {
			lastResolved = note.UpdatedAt
		}
	}
	if time.Since(lastResolved) < threshold {
		return Check{description, false, fmt.Sprintf("Threads have not been resolved for enough time (last resolved: %s)", lastResolved), nil}
	}
	return Check{
		description,
		true,
		"No unresolved thread has been found",
		nil,
	}
}

// IsWelcomeMessagePresent detects if the bot already wrote a Welcome message
func (mr *MergeRequest) IsWelcomeMessagePresent() (bool, error) {
	notes, err := mr.GetAllNotes(false)
	if err != nil {
		return false, err
	}
	for _, note := range *notes {
		var metadata Metadata
		found, err := retrieveMetadataFromString(note.Body, &metadata)
		if err != nil {
			return false, err
		}
		if found && metadata.MessageType == messageTypeWelcome {
			return true, nil
		}
	}
	return false, nil
}

// GenerateWelcomeMessage a Welcome Message in Markdown
func (mr *MergeRequest) GenerateWelcomeMessage() (string, error) {
	message := fmt.Sprintln("### Thanks")
	message += fmt.Sprintln("Thanks for your contribution!")
	message += fmt.Sprintln("")
	message += fmt.Sprintln("When all the following conditions are fulfilled, your MergeRequest will be reviewed by the Team:")
	message += fmt.Sprintln("- the check pipeline pass")
	message += fmt.Sprintln("- the MR is considered as 'mergeable' by gitlab")
	authorIsDev, err := mr.IsAuthorDevOrHigher()
	if err != nil {
		return "", err
	}
	message += fmt.Sprintln("")
	message += fmt.Sprintln("<details><summary>Click here for more details about the acceptance conditions</summary><p>")
	message += fmt.Sprintln("")
	message += fmt.Sprintln("### Acceptance conditions")
	message += fmt.Sprintln("Your MergeRequest will be accepted if:")
	if authorIsDev {
		message += fmt.Sprintln("- There is no activity on this MergeRequest for at least ", mrInactiveHours, "h")
		message += fmt.Sprintln("")
		message += fmt.Sprintln("*OR*")
		message += fmt.Sprintln("")
		message += fmt.Sprintln("- All discussions have been considered resolved (and closed) for at least ", mrDevDiscussionHours, "h")
		message += fmt.Sprintln("- The developers vote balance (", ":"+VoteUp+":", "/", ":"+VoteDown+":", ") is positive for at least", mrDevScoreHours, "h")
	} else {
		message += fmt.Sprintln("- All discussions have been considered resolved (and closed) for at least ", mrOtherDiscussionHours, "h")
		message += fmt.Sprintln("- The developers vote balance (", ":"+VoteUp+":", "/", ":"+VoteDown+":", ") is strictly positive for at least", mrOtherScoreHours, "h")
	}
	message += fmt.Sprintln("</p></details>")
	// Add messagetype in metadata
	meta, err := generateMetadataString(Metadata{MessageType: messageTypeWelcome})
	if err != nil {
		return "", err
	}
	message += fmt.Sprintln("<!--")
	message += fmt.Sprintln(meta)
	message += fmt.Sprintln("-->")
	return message, nil
}

// GetAllNotesReferingUser retrieve all notes refering to a specific user in their body
func (mr *MergeRequest) GetAllNotesReferingUser(user User) (*[]Note, error) {
	allNotes, err := mr.GetAllNotes(false)
	if err != nil {
		return allNotes, err
	}
	notes := []Note{}
	keyword := "@" + user.Username
	// as @ is not ascii character, you cannot directly use \b as separator for a word.
	r, err := regexp.Compile(`(?:\A|\z|\s)(` + keyword + `)(?:[[:punct:]]|\s|\z)`)
	if err != nil {
		return nil, err
	}
	for _, note := range *allNotes {
		if r.MatchString(note.Body) {
			notes = append(notes, note)
		}
	}
	return &notes, nil
}

// CheckPipelines checks last pipeline of MR is successful
func (mr *MergeRequest) CheckPipelines() Check {
	description := "Last pipeline should be successful"
	headPipeline, err := mr.GetHeadPipeline()
	if err != nil {
		return Check{description, false, "", err}
	}
	if headPipeline == nil {
		return Check{description, false, "No pipeline has been found", nil}
	}
	return Check{
		description,
		headPipeline.Status == PipelineStatusSuccess,
		fmt.Sprintf("Last Pipeline (%d) has status %s", headPipeline.ID, headPipeline.Status),
		nil,
	}
}

// CheckAuthorIsDev checks if the author has a developer access or higher
// This is an optional check that is used to validate the "Inactivity" scenario
func (mr *MergeRequest) CheckAuthorIsDev() Check {
	description := "Check whether MR author has developer access or higher"
	authorIsDev, err := mr.IsAuthorDevOrHigher()
	if err != nil {
		return Check{description, false, "", err}
	}
	if authorIsDev {
		return Check{description, true, "Author has developer access or higher", nil}
	}
	return Check{description, false, "Author does NOT have developer access or higher", nil}
}

// Analyze generates a the analysis report of the MR
// A "report" is a list of checks
func (mr *MergeRequest) Analyze() []Check {
	checks := []Check{}
	// MANDATORY checks
	checks = append(checks, mr.CheckStatus())
	checks = append(checks, mr.CheckPipelines())
	// Checks that depend on author role (>= developer or not)
	devCheck := mr.CheckAuthorIsDev()
	if devCheck.Error != nil {
		// Unable to determine the author role, aborting.
		checks = append(checks, devCheck)
		return checks
	}
	if devCheck.Success {
		// Author is >= developer
		checks = append(checks, devCheck)
		// Special Case: inactivity on a developer MR
		inactiveCheck := mr.CheckInactivity(time.Duration(mrInactiveHours) * time.Hour)
		if inactiveCheck.Success {
			checks = append(checks, inactiveCheck)
			return checks
		}
		logMRVerbose(mr.IID, "[OPTIONAL] ", inactiveCheck.String())
		checks = append(checks, mr.CheckDevScore(time.Duration(mrDevScoreHours)*time.Hour, false))
		checks = append(checks, mr.CheckDiscussions(time.Duration(mrDevDiscussionHours)*time.Hour))
	} else {
		checks = append(checks, mr.CheckDevScore(time.Duration(mrOtherScoreHours)*time.Hour, true))
		checks = append(checks, mr.CheckDiscussions(time.Duration(mrOtherDiscussionHours)*time.Hour))
	}
	return checks
}
